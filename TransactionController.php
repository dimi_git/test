<?php

namespace app\Http\Controllers;
class TransactionController extends Controller
{
    public function transaction(Request $request)
    {
        db::beginTransaction();
        $user = User::find($request->user_id);
        if($request->trans_type === 'income')
            $user->balans = $user->balans + $request->trans_sum;
        else
            $user->balans = $user->balans - $request->trans_sum;
        $user->save();

        Transaction::create([
            "user" => $request->user_id,
            "trans_type" => $request->trans_type,
            "trans_sum" => $request->trans_sum,
        ]);
        db::commit();

    }
}
